var exec = require('cordova/exec');

var getReaderTypes = function() {
	if (device.platform == 'Android') {
		return {
			unimag: 'UM',
			unimag_pro: 'UM_PRO',
			unimag_ii: 'UM_II',
			shuttle: 'SHUTTLE'
		};
	} else {
		return {
			unimag: 'UMREADER_UNIMAG',
			unimag_pro: 'UMREADER_UNIMAG_PRO',
			unimag_ii: 'UMREADER_UNIMAG_II',
			shuttle:'UMREADER_SHUTTLE'
		};
	}
};

var Swiper = function() {};

Swiper.activate = function(success, error) {
	exec(success, error, 'UnimagSwiper', 'activateReader', []);
};

Swiper.deactivate = function(success, error) {
	exec(success, error, 'UnimagSwiper', 'deactivateReader', []);
};

Swiper.swipe = function (success, error) {
	exec(success, error, 'UnimagSwiper', 'swipe', []);
};

Swiper.enableLogs = function (enable, success, error) {
	exec(success, error, 'UnimagSwiper', 'enableLogs', [enable]);
};

Swiper.setReaderType = function (type, success, error) {
	var readerType = getReaderTypes()[type];
	if (readerType) {
		exec(success, error, 'UnimagSwiper', 'setReaderType', [readerType]);
	} else console.log('Could not set reader type - invalid type "' + type + '" provided.');
};

Swiper.autoConfig = function (success, error) {
	if (device.platform == 'Android') {
		exec(success, error, 'UnimagSwiper', 'autoConfig', []);
	}
};

Swiper.fireEvent = function (event, data) {
	if (event == 'swipe_succes') {
		var base64data = data.detail;
		var hex = base64toHEX(base64data);
		var parsed = parseSwipeData(hex)
		data.parsed = parsed;
	}
	var customEvent = new CustomEvent(event, { 'detail': data} );
	window.dispatchEvent(customEvent);
};

Swiper.on = function (event, callback, scope) {
	window.addEventListener(event, callback.bind(scope || window));
};

var hexToAscii = function (a) {
	var b = [];
	for (var c = 0; c < a.length; c=c+2) {
			b.push(a.slice(c,c+2));
	}
	return hexArrayToAscii(b);
}
var hexArrayToAscii = function (a) {
	var str = '';
	for (var n = 0; n < a.length; n++) {
			str += String.fromCharCode(parseInt(a[n], 16));
	}
	return str;
}
var base64toHEX = function (base64) {
	var raw = atob(base64);
	var HEXstr = '';
	var HEXArray = []
	for (i = 0; i < raw.length; i++) {
			var _hex = raw.charCodeAt(i).toString(16)
			HEXstr += (_hex.length == 2 ? _hex : '0' + _hex);
			HEXArray.push((_hex.length == 2 ? _hex : '0' + _hex).toUpperCase())
	}

	return { hexString: HEXstr.toUpperCase(), hexArray: HEXArray }
}
function hexToBase64(str) {
	return btoa(String.fromCharCode.apply(null,
			str.replace(/\r|\n/g, "").replace(/([\da-fA-F]{2}) ?/g, "0x$1 ").replace(/ +$/, "").split(" "))
	);
}

var parseSwipeData = function (hex) {
	var errorText;
	// more infor here 
	// https://idtechproducts.com/how-to-parse-magstripe-credit-card-data/
	// https://atlassian.idtechproducts.com/confluence/display/KB/UniMag+II+(ID-80110008-xxx)+-+Home
	// 
	// byte 0 = STX	02 / start byte
	// byte 1+2 = LENGTH	ED 01
	// byte 3 = 80 (card type 80 = financial card)
	// byte 4 = track status 
	// 0-------  0 Reserved for future use
	// -0------  1: Field 10  optional bytes length  exists (0: No Field 10)
	// --0-----  1: Track 3 sampling data exists (0: Track 3 sampling data does not exist)
	// ---1----  1: Track 2 sampling data exists (0: Track 2 sampling data does not exist)
	// ----1---  1: Track 1 sampling data exists (0: Track 1 sampling data does not exist)
	// -----1--  1: Track 3 decode success (0: Track 3 decode fail)
	// ------1-  1: Track 2 decode success (0: Track 2 decode fail)
	// -------1  1: Track 1 decode success (0: Track 1 decode fail)
	// byte 5 = track 1 length 
	// byte 6 = track 2 length  <-- we need this! 
	// byte 7 = track 3 length
	try {
			var t1Length = parseInt(hex.hexArray[5], 16);
			var t2Length = parseInt(hex.hexArray[6], 16);
			var t3Length = parseInt(hex.hexArray[7], 16);
			if (t2Length == 0) {
					return { unimag: { err: "Failed parsing magnetic swipe data (2), swipe again please" } };
			} else {
					// try to grab these track 2 bytes 
					var r = {};

					// byte 8 = clear mask data
					// Clear/Mask Data Sent Status (83)	
					// 1-------  Bit 7: 1   Serial Number present; 0 not present
					// -0------  Bit 6: 1  PIN Encryption Key; 0 Data Encryption Key
					// --0-----  Bit 5: 1  Chip present on card. (First byte of service code was '2' or '6'.) Use EMV transaction if possible.
					// ---0----  Bit 4: 0   TDES; 1   AES
					// ----0---  Bit 3: 1  if fixed key; 0 DUKPT Key Management
					// -----0--  Bit 2: 1  if Track3 clear/mask data present
					// ------1-  Bit 1: 1  if Track2 clear/mask data present
					// -------1  Bit 0: 1  if Track1 clear/mask data present
					var byte8 = hex.hexArray[8]
					var bits8 = parseInt(byte8, 16).toString(2)
					bits8 = '0'.repeat(8 - bits8.length) + bits8
					r.clearMaskData = bits8;

					if (bits8.slice(3, 4) == '0') r.algorithm = 'TDES';
					if (bits8.slice(3, 4) == '1') r.algorithm = 'AES';

					//byte 9 = encryption data
					// Encrypted/Hash Data Sent Status (9B)	
					// 1-------  Bit 7: if 1, KSN present
					// -0------  Bit 6: if 1, session ID present 
					// --0-----  Bit 5: if 1, track3 hash data (SHA digest) present
					// ---1----  Bit 4: if 1, track2 hash data (SHA digest) present
					// ----1---  Bit 3: if 1, track1 hash data (SHA digest) present
					// -----0--  Bit 2: if 1, track3 encrypted data present
					// ------1-  Bit 1: if 1, track2 encrypted data present
					// -------1  Bit 0: if 1, track1 encrypted data present
					var byte9 = hex.hexArray[9]
					var bits9 = parseInt(byte9, 16).toString(2)
					bits9 = '0'.repeat(8 - bits9.length) + bits9
					r.encryptiondata = bits9;
					if (bits9.slice(0, 1) == '1') r.KSNPresent = true;
					if (bits9.slice(5, 6) == '1') r.t3encryptedPresent = true;
					if (bits9.slice(6, 7) == '1') r.t2encryptedPresent = true;
					if (bits9.slice(7, 8) == '1') r.t1encryptedPresent = true;

					var offset = 10;
					if (t1Length > 0) {
							var track1 = hex.hexArray.slice(offset, offset + t1Length);
							offset += t1Length;
							r.track1 = hexArrayToAscii(track1);
					}
					if (t2Length > 0) {
							var track2 = hex.hexArray.slice(offset, offset + t2Length);
							offset += t2Length;
							r.track2 = hexArrayToAscii(track2);
					}
					if (t3Length > 0) {
							var track3 = hex.hexArray.slice(offset, offset + t3Length);
							offset += t3Length;
							r.track3 = hexArrayToAscii(track3);
					}
					var multiples = 8;
					if (r.algorithm == 'AES') multiples = 16;
					if (r.t1encryptedPresent) {
							var l = Math.ceil(t1Length / multiples) * multiples;
							r.track1Encrypted = hex.hexArray.slice(offset, offset + l);
							offset += l;
					}
					if (r.t2encryptedPresent) {
							var l = Math.ceil(t2Length / multiples) * multiples;
							r.track2Encrypted = hex.hexArray.slice(offset, offset + l);
							r.track2EncryptedBase64 = hexToBase64(r.track2Encrypted.join(''));
							offset += l;
					} else {
							errorText = "Failed parsing magnetic swipe data (code: 1), swipe again please";
					}
					if (r.t3encryptedPresent) {
							var l = Math.ceil(t3Length / multiples) * multiples;
							r.track3Encrypted = hex.hexArray.slice(offset, offset + l);
							offset += l;
					}
					r.readerSerial = hex.hexArray.slice(offset, offset + 10);
					offset += 10;
					if (r.KSNPresent) {
							r.KSN = hex.hexArray.slice(offset, offset + 10);
							offset += 10;        
					} else {
							errorText = "Failed parsing magnetic swipe data (code: 2), swipe again please";
					}
					if (errorText) {
							return { unimag: { data: r, err: errorText } };
					} else {
							return { unimag: { data: r } };
					}
			}
	} catch (e) {
			console.error(e);
			return { unimag: { err: "Failed parsing magnetic swipe data (code: 9), swipe again please" } };
	}

}

/*
 * Polyfill for adding CustomEvent -- Copy uncommented lines below into your
 * application if you get  Reference Error: CustomEvent is undefined
 * see : https://developer.mozilla.org/fr/docs/Web/API/CustomEvent,
         http://stackoverflow.com/questions/25579986/

	if (!window.CustomEvent) { // Create only if it doesn't exist
	    (function () {
	        function CustomEvent ( event, params ) {
	            params = params || { bubbles: false, cancelable: false, detail: undefined };
	            var evt = document.createEvent( 'CustomEvent' );
	            evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
	            return evt;
	        };
	
	        CustomEvent.prototype = window.Event.prototype;
	
	        window.CustomEvent = CustomEvent;
	    })();
	}
*/



module.exports = Swiper;

